# UI Automation Primer Demo (iOS) #

### What is this repository for? ###

* Demo app to be used in conjunction with corresponding article / white paper.
* Version 1.0

### How do I get set up? ###

* Xcode 6.0+ (iOS project).
* Instruments (script file zipped within the project).
* See article / white paper for details on how to run scripts/

### Contribution guidelines / Contact ###

* All feedback / contributions welcome, either through git, [blog](http://www.mrchrisbarker.co.uk) or [twitter](http://twitter.com/MrChrisBarker)