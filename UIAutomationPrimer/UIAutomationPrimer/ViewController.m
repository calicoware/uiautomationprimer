//
//  ViewController.m
//  UIAutomationPrimer
//
//  Created by MacbookPro on 07/12/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *g1 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeUp:)];
    [g1 setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:g1];
    
    UIPinchGestureRecognizer *p1 = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinch:)];
    [self.view addGestureRecognizer:p1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pinch:(UIPinchGestureRecognizer *)gesture{
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Pinched me" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
}

-(void)swipeUp:(UISwipeGestureRecognizer *)gesture{
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Swipe Up!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
}
- (IBAction)btnAlert:(id)sender {
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This is an Alert" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [av show];
    
}

@end
