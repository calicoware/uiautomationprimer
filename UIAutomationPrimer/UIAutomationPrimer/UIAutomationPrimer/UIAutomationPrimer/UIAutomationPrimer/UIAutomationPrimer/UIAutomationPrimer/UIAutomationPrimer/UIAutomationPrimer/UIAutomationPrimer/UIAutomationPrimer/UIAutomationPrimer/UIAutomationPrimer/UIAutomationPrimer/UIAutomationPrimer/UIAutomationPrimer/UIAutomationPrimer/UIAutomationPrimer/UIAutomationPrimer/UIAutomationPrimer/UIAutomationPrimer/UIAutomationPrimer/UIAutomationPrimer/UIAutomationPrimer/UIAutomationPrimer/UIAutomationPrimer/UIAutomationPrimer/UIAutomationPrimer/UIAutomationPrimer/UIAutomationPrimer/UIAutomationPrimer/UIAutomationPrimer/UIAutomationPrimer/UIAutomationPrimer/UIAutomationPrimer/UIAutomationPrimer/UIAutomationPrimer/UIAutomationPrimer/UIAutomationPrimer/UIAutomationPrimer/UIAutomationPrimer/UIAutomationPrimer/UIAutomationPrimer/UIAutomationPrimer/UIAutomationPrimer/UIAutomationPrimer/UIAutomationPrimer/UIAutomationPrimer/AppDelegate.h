//
//  AppDelegate.h
//  UIAutomationPrimer
//
//  Created by MacbookPro on 07/12/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

