//
//  main.m
//  UIAutomationPrimer
//
//  Created by MacbookPro on 07/12/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
